const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/booksDB', { useNewUrlParser: true, useUnifiedTopology: true },
    err => {
        if (!err)
            console.log('Database Connection Successfull.')
        else
            console.log('Error connecting database : ' + JSON.stringify(err, undefined, 2))
    })