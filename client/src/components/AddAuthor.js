import React from 'react';
import { graphql } from 'react-apollo';
import { flowRight as compose } from 'lodash';
import { getAuthorsQuery, addAuthorMutation } from '../queries/queries';
import useForm from './useForm';

const initialFieldValues = {
    name: '',
    age: '',
}

const AddAuthor = (props) => {
    var { values, handleInputChange, resetForm } = useForm(initialFieldValues);

    const handleSubmit = e => {
        e.preventDefault();

        props.addAuthorMutation({
            variables: {
                name: values.name,
                age: parseInt(values.age)
            },

            refetchQueries: [{ query: getAuthorsQuery }]
        });

        resetForm();
    }

    return (
        <form id="AddAuthor" onSubmit={handleSubmit}>
            <div className="field">
                <label>Author name</label>
                <input type="text" name="name" placeholder="enter author name" onChange={handleInputChange} value={values.name} />
            </div>

            <div className="field">
                <label>Author Age</label>
                <input type="number" name="age" placeholder="enter author age" onChange={handleInputChange} value={values.age} />
            </div>

            <div className="field">
                <button> + </button>
            </div>
        </form>
    )
};

export default compose(
    graphql(addAuthorMutation, { name: "addAuthorMutation" })
)(AddAuthor);