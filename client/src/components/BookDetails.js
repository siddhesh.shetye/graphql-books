import React from 'react';
import { graphql } from 'react-apollo';
import { flowRight as compose } from 'lodash';
import { getBookQuery, getBooksQuery, deleteBookMutation } from '../queries/queries';

const BookDetails = (props) => {
    const onDelete = id => {
        if (window.confirm('Are you sure you want to delete ?')) {

            props.deleteBookMutation({
                variables: {
                    id: id
                },

                refetchQueries: [{ query: getBooksQuery }],
            });
        }
    }

    const displayBookDetails = () => {
        const { book } = props.data;
        if (book && props.currentBookId !== 0) {
            return (
                <div>
                    <h2>{book.name}</h2>
                    <p>{book.genre}</p>
                    <p>{book.author.name}</p>
                    <p>All books by this author:</p>
                    <ul className="other-books">
                        {book.author.books.map(item => {
                            return <li key={item.id}>{item.name}</li>
                        })}
                    </ul>

                    <div>
                        <button className="btn-delete btn-primary" onClick={() => onDelete(book.id)}> Delete </button>
                    </div>
                </div>
            );
        } else {
            return (<div>No book selected...</div>);
        }
    }


    return (
        <div id="book-details">
            {displayBookDetails()}
        </div>
    )
};

export default compose(
    graphql(getBookQuery, {
        options: (props) => {
            return {
                variables: {
                    id: props.currentBookId
                }
            }
        }
    }, { name: "getBookQuery" }),
    graphql(deleteBookMutation, { name: "deleteBookMutation" })
)(BookDetails);