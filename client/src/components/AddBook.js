import React from 'react';
import { graphql } from 'react-apollo';
import { flowRight as compose } from 'lodash';
import { getAuthorsQuery, addBookMutation, getBooksQuery } from '../queries/queries';
import useForm from './useForm';

const initialFieldValues = {
    name: '',
    genre: '',
    authorid: ''
}

const AddBook = (props) => {
    var { values, handleInputChange, resetForm } = useForm(initialFieldValues);

    const handleSubmit = e => {
        e.preventDefault();

        props.addBookMutation({
            variables: {
                name: values.name,
                genre: values.genre,
                authorid: values.authorid
            },

            refetchQueries: [{ query: getBooksQuery }]
        });

        resetForm();
    }

    return (
        <form id="AddBook" onSubmit={handleSubmit}>
            <div className="field">
                <label>Book name</label>
                <input type="text" name="name" placeholder="enter book name" onChange={handleInputChange} value={values.name} />
            </div>

            <div className="field">
                <label>Genre</label>
                <input type="text" name="genre" placeholder="enter genre" onChange={handleInputChange} value={values.genre} />
            </div>

            <div className="field">
                <label>Author</label>
                <select name="authorid" onChange={handleInputChange} value={values.authorid}>
                    <option>Select author</option>
                    {
                        props.getAuthorsQuery.loading ? <option> Loading...</option> : props.getAuthorsQuery.authors.map(author => {
                            return (
                                <option key={author.id} value={author.id}> {author.name} </option>
                            );
                        })
                    }
                </select>
            </div>

            <div className="field">
                <button className="btn-primary"> + </button>
            </div>
        </form>
    )
};

export default compose(
    graphql(getAuthorsQuery, { name: "getAuthorsQuery" }),
    graphql(addBookMutation, { name: "addBookMutation" })
)(AddBook);