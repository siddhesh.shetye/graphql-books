import React, { useState, useEffect } from 'react';
import { graphql } from 'react-apollo';
import Loading from '../layouts/Loading';
import { getBooksQuery } from '../queries/queries';
import BookDetails from './BookDetails';

const BookList = (props) => {
    const [currentBookId, setCurrentBookId] = useState(0);

    useEffect(() => {

        //set currentBookId to 0 if book deleted and not found in list
        if (props.data.books) {
            let notFound = props.data.books.find(x => x.id === currentBookId);
            if (!notFound) {
                setCurrentBookId(0);
            }
        }
    })

    return (
        <div>
            <ul id="book-list">
                {
                    props.data.loading ? <Loading /> : props.data.books.map(book => {
                        return (
                            <li key={book.id} onClick={() => setCurrentBookId(book.id)}> {book.name} </li>
                        );
                    })
                }
            </ul>
            <BookDetails currentBookId={currentBookId} />
        </div>
    )
};

export default graphql(getBooksQuery)(BookList);