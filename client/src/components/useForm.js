import { useState } from 'react';

const useForm = (initialFieldValues) => {

    const [values, setValues] = useState(initialFieldValues);

    const handleInputChange = e => {
        const { name, value } = e.target;

        setValues({
            ...values,
            [name]: value
        })
    }

    const resetForm = () => {
        setValues(initialFieldValues);
    }

    return {
        values,
        setValues,
        handleInputChange,
        resetForm
    };
}

export default useForm;