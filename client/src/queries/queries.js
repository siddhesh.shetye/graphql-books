import { gql } from 'apollo-boost';

const getBooksQuery = gql`
{
    books {
        name
        id
    }
}
`

const getAuthorsQuery = gql`
{
    authors {
        name
        id
    }
}
`

const addBookMutation = gql`
    mutation($name: String!, $genre: String!, $authorid: ID! ) {
        addBook(name: $name, genre: $genre, authorid: $authorid){
            name
            id
        }
    }
`

const addAuthorMutation = gql`
    mutation($name: String!, $age: Int! ) {
        addAuthor(name: $name, age: $age){
            name
            age
        }
    }
`

const deleteBookMutation = gql`
    mutation($id: ID!) {
        deleteBook(id: $id){
            name
        }
    }
`

const getBookQuery = gql`
    query ($id: ID){
        book(id: $id) {
            id
            name
            genre
            author {
                id
                name
                age
                books {
                    name
                    id
                }
            }
        }
    }
`;

export { getBooksQuery, getAuthorsQuery, addBookMutation, addAuthorMutation, getBookQuery, deleteBookMutation };